# ESP32 bass synth

ESP32 based monosynth for embedding into electric bass guitar

### TODO

* find some good name
* pitch tracking
* bluetooth configuration
* bandlimited oscillators
* better filter
* envelope
* modulations
* OLED GUI
* ...

