#ifndef __WS_HANDLER__
#define __WS_HANDLER__

#include "ESPAsyncWebServer.h"

void ws_handler(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len);

#endif  // __WS_HANDLER__