#ifndef __LIMITER_H__
#define __LIMITER_H__

#include <math.h>

// Simple output limiter
//
// code from http://musicdsp.org/showone.php?id=120
//
// This file is part of esp32 bass synth project
// (c)2018 Igor Brkic <igor@hyperglitch.com>

class Limiter{
  static inline float sigmoid(float x){
      if(fabs(x)<1)
          return x*(1.5f - 0.5f*x*x);
      else
          return x > 0.f ? 1.f : -1.f;
  }
public:
  static inline float run(float x, float t){
      if(x<t && x>-t)
          return x;
      else{
          if(x > 0.f){
              return t + (1.f-t)*sigmoid((x-t)/((1-t)*1.5f));
          }
          else{
              return -(t + (1.f-t)*sigmoid((-x-t)/((1-t)*1.5f)));
          }
      }
  }
};

#endif  // __LIMITER_H__