#include "Arduino.h"
#include "driver/i2s.h"
#include "SSD1306.h"
#include <WiFi.h>

#include "FS.h"
#include "SPIFFS.h"

#include "ESPAsyncWebServer.h"

#include "html_page.h"

#include "font.h"
#include "oscillator.h"
#include "envelope.h"
#include "filter.h"
#include "limiter.h"

#include "websocket_handler.h"

// pin, defines, globals and struct definitions
#include "common.h"

// ------------------  globals
SSD1306 display(0x3c, pin_display_sda, pin_display_scl);

volatile uint8_t waveform[2][128];
volatile int waveform_idx = 0;

int16_t pitch_buffer[2][PITCH_TRACKER_BLOCK_SIZE];
int pitch_idx = 0;
int pitch_block_idx = 0;

volatile synth_data_t synth_config;

hw_timer_t * adc_timer = NULL;

volatile float input_frequency = 0;
volatile float input_tmp = 0;
volatile bool input_onset = false;

// Web
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

// UI
volatile int pot = 0; // tmp
static const int pot_range[2] = {2250, 4095};
volatile bool button_state = false;

//i2s configuration 
int i2s_num = 0; // i2s port number
i2s_config_t i2s_config = {
    .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_DAC_BUILT_IN),
    .sample_rate = SAMPLE_RATE,
    .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT, // the DAC module will only take the 8bits from MSB 
    .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
    .communication_format = (i2s_comm_format_t)I2S_COMM_FORMAT_I2S_MSB,
    .intr_alloc_flags = 0, // default interrupt priority
    .dma_buf_count = 8,
    .dma_buf_len = 64,
    .use_apll = 0
};


static TaskHandle_t pitch_task_to_notify = NULL;
void IRAM_ATTR adc_update(void){
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;
  pitch_buffer[pitch_block_idx][pitch_idx] = analogRead(pin_sound_input);
  pitch_idx++;
  if(pitch_idx==PITCH_TRACKER_BLOCK_SIZE){
    pitch_idx = 0;
    pitch_block_idx = 1-pitch_block_idx;
    vTaskNotifyGiveFromISR(pitch_task_to_notify, &xHigherPriorityTaskWoken);
  }
}

void set_bit(uint32_t *bits, uint32_t i, uint32_t val){
  uint32_t mask = 1 << (i % NBITS);
  uint32_t *ref = bits+ (i / NBITS);
  *ref ^= (-val ^ *ref) & mask;
}

uint32_t count_bits(uint32_t i){
   // GCC only!!!
   return __builtin_popcount(i);
}

/* median and helpers */
#define SORT(a, b) if(a>b) SWAP(a,b);
#define SWAP(a, b) (((a)=(a)+(b)), ((b)=(a)-(b)), ((a)=(a)-(b)))
float medfilt5(float *input){
  float a, b, c, d, e;
  a=input[0];
  b=input[1];
  c=input[2];
  d=input[3];
  e=input[4];

  // makes a < b and c < d
  SORT(a, b);
  SORT(c, d);

  // eliminate the lowest
  if(c < a){
    SWAP(b, d);
    c=a;
  }

  // gets e in
  a=e;

  // makes a < b
  SORT(a, b);

  // eliminate another lowest
  // remaing: a,b,d
  if(a<c){
    SWAP(b, d);
    a=c;
  }

  return (d<a)?d:a;
}


void pitch_tracker(void * parameter){
  // bitstream autocorrelation algorithm by Joel de Guzman
  // http://www.cycfi.com/2018/03/fast-and-efficient-pitch-detection-bitstream-autocorrelation/

  const int buff_size = 2*PITCH_TRACKER_BLOCK_SIZE; // must be power of 2
  const float max_freq = 400; // input should additionaly be filtered to prevent any frequency above ADC_SAMPLE_RATE/2
  const float min_period = (float)ADC_SAMPLE_RATE / max_freq;
  float buff[buff_size];
  uint32_t bits[buff_size/NBITS];

  // DC blocker memory
  float dcb_xm1 = 4096.0/2.0;
  float dcb_ym1 = 0;
  const float dcb_coeff = 0.99;

  // median buffer
  const int medfilt_size = 5;
  float medfilt_buff[medfilt_size] = {0,0,0,0,0};
  int medfilt_idx = 0;

  // clear the input buffer
  for(int i=0; i<buff_size; i++) buff[i]=0.0;

  pitch_task_to_notify = xTaskGetCurrentTaskHandle();
  while(true){
    // wait for buffers to fill up
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

    // shift old data to the first half of the buffer
    memcpy(buff, buff+PITCH_TRACKER_BLOCK_SIZE, PITCH_TRACKER_BLOCK_SIZE*sizeof(float));

    // copy new data to second half of the buffer (with DC blocking applied)
    for(int i=0; i<PITCH_TRACKER_BLOCK_SIZE; i++){
      const float in = (float)pitch_buffer[1-pitch_block_idx][i];
      const float out = in-dcb_xm1 + dcb_coeff*dcb_ym1;
      dcb_xm1 = in;
      dcb_ym1 = out;
      buff[i+PITCH_TRACKER_BLOCK_SIZE] = out/4096.0;
    }

    // reset the bits buffer
    for(int i=0; i<buff_size/NBITS; i++) bits[i]=0;

    // do the zero-crossing detection and set bits
    // TODO: replace this with peak trigger
    uint32_t y = 0;
    for(int i=0; i<buff_size; i++){
      if(buff[i]<-0.001) y=0;
      else if(buff[i]>0) y=1;
      set_bit(bits, i, y);
    }

    //binary autocorrelation
    uint32_t max_count = 0;
    uint32_t min_count = UINT32_MAX;
    size_t est_index = 0;
    int tmp = 0;
    uint32_t corr[buff_size/2];

    const uint32_t start_pos = min_period;

    int mid_array = buff_size/NBITS/2-1;
    int mid_pos = buff_size / 2;
    int index = start_pos/NBITS;
    int shift = start_pos % NBITS;

    for(int pos=start_pos; pos!=mid_pos; pos++){
      uint32_t *p1 = bits;
      uint32_t *p2 = bits+index;
      int count = 0;

      if(shift==0){
        for(int i=0; i!=mid_array; i++){
          count += count_bits(*p1++ ^ *p2++);
        }
      }
      else{
        int shift2 = NBITS - shift;
        for(int i=0; i!=mid_array; i++){
          uint32_t v = *p2++ >> shift;
          v |= *p2<<shift2;
          count += count_bits(*p1++^v);
        }
      }
      shift++;
      if(shift==NBITS){
        shift = 0;
        index++;
      }

      corr[pos] = count;
      if(count>max_count) max_count = count;
      if(count<min_count){
        min_count = count;
        est_index = pos;
        tmp = count;
      }

    }

    // handle harmonics
    float sub_threshold = 0.15*max_count;
    int max_div = est_index / min_period;
    for(int div=max_div; div!=0; div--){
      bool all_strong = true;
      float mul = 1.0/div;
      for(int k=1; k!=div; k++){
        int sub_period = k*est_index*mul;
        if(corr[sub_period]>sub_threshold){
          all_strong = false;
          break;
        }
      }
      if(all_strong){
        est_index = est_index*mul;
        break;
      }
    }

    // estimate the pitch
    // Get the start edge
    float prev = 0;
    int start_edge_idx = 0;
    for (; buff[start_edge_idx] <= 0.0f; ++start_edge_idx)
      prev = buff[start_edge_idx];
    float dy = buff[start_edge_idx] - prev;
    float dx1 = -prev / dy;
    // Get the next edge
    int next_edge_idx = 0 + est_index - 1;
    for (; buff[next_edge_idx] <= 0.0f; ++next_edge_idx)
      prev = buff[next_edge_idx];
    dy = buff[next_edge_idx] - prev;
    float dx2 = -prev / dy;

    float n_samples = (next_edge_idx - start_edge_idx) + (dx2 - dx1);
    float est_freq = (float)ADC_SAMPLE_RATE / n_samples;

   // TODO: handle onset, add median, ...
    /*
    if(tmp>270){
      printf("tmp: %d\tf: %.2f\n", tmp, est_freq);
    }
    else{
      printf("---\n");
    }
    */
    medfilt_buff[medfilt_idx] = est_freq;
    medfilt_idx = (medfilt_idx+1)%5;
    if(!button_state){
      input_frequency = medfilt5(medfilt_buff);
      input_tmp = tmp;
    }

  }
  vTaskDelete(NULL);
}


void block_processor(void * parameter){
  uint8_t dac_out[BLOCK_SIZE];  // 8bit DAC
  int out_idx = 0;

  Oscillator osc1(SAMPLE_RATE, 33, OscillatorShape::SAWTOOTH, 0.6);
  Oscillator osc2(SAMPLE_RATE, 0, OscillatorShape::SAWTOOTH, 0.4);
  osc2.set_detune(0.07);
  bool hard_sync = false;

  Oscillator lfo1(SAMPLE_RATE, 0.7, OscillatorShape::SINE);

  // modulation intensities
  float lfo1_osc1_f_int = 0.0;
  float lfo1_osc1_v_int = 0.0;
  float lfo1_osc2_f_int = 0.0;
  float lfo1_osc2_v_int = 0.0;
  float lfo1_cutoff_int = 0.0;
  float lfo1_reso_int = 0.0;

  Envelope env(SAMPLE_RATE);

  Filter filter(0.5, 0.1);
  float filter_envmod = 0.0;

  // add pointers to global object to enable parameter change from outside
  synth_config.osc1 = &osc1;
  synth_config.osc2 = &osc2;
  synth_config.lfo1 = &lfo1;
  synth_config.lfo1_osc1_f_int = &lfo1_osc1_f_int;
  synth_config.lfo1_osc1_v_int = &lfo1_osc1_v_int;
  synth_config.lfo1_osc2_f_int = &lfo1_osc2_f_int;
  synth_config.lfo1_osc2_v_int = &lfo1_osc2_v_int;
  synth_config.lfo1_cutoff_int = &lfo1_cutoff_int;
  synth_config.lfo1_reso_int   = &lfo1_reso_int;
  synth_config.filter_envmod   = &filter_envmod;

  synth_config.env = &env;
  synth_config.filter = &filter;
  synth_config.hard_sync = &hard_sync;

  int waveform_cycle = 0;
  int waveform_sample_idx = 0;
  while(true){
    float out = 0;

    osc1.oscillate();
    if(hard_sync){
      if(osc1.wrapped) osc2.reset();
    }
    osc2.oscillate();

    lfo1.oscillate();

    float perc = (lfo1.out+1)/2;

    // mixer
    //out = osc1.out*perc + osc2.out*(1-perc);
    out = osc1.out + osc2.out;

    // apply envelope
    out *= env.process();

    // add filter
    //filt.set_reso((float)pot/4095.0);
    //filter.set_cutoff((float)pot/4095.0/8);
    if(pot>=pot_range[0] && pot<=pot_range[1]){
      filter.set_cutoff((pot-pot_range[0])/(float)(pot_range[1]-pot_range[0])/4.0);
    }
    else{
      filter.set_cutoff(0.5);
    }
    out = filter.run(out);

    // add limiter/saturation
    out = Limiter::run(out, 0.7);

    // waveform display
    if(osc1.wrapped && waveform_sample_idx==128){
      // switch ui buffers
      waveform_idx = 1-waveform_idx;
      waveform_sample_idx = 0;
      waveform_cycle = 0;
    }

    if(waveform_cycle==10){
      waveform_cycle = 0;
      if(waveform_sample_idx<128){
        waveform[waveform_idx][waveform_sample_idx++] = (int16_t)(out * 127.0)+128;
      }
    }
    else{
      waveform_cycle++;
    }

    // send output to DAC and update controls/modulations
    dac_out[out_idx++] = (int8_t)((int16_t)(out * 127.0) + 128);
    if(out_idx == BLOCK_SIZE){
      out_idx = 0;
      // i2s will block until all samples are written to DAC buffer
      (void)i2s_write_bytes((i2s_port_t)i2s_num, (const char *)dac_out, BLOCK_SIZE, 100);

      // update controls @ SAMPLE_RATE/BLOCK_SIZE frequency (enough not to be noticable)

      const float lfo1_out = (lfo1.out+1.0)/2.0;  // scale it to the [0,1] range
      //osc1.set_freq((float)pot/8);  // FIXME: remove

      if(input_tmp<30){
        int n = MidiConversion::get_note(input_frequency);
        osc1.set_note(n);
        osc2.set_note(osc1.get_note());
      }
      if(input_tmp<30 && (env.get_state()==EnvelopeState::IDLE || env.get_state()==EnvelopeState::RELEASE)){
        env.gate(true);
      }
      else if(input_tmp>60 && env.get_state()!=EnvelopeState::IDLE && env.get_state()!=EnvelopeState::RELEASE && !button_state){
        env.gate(false);
      }
      osc1.set_freq_mod(lfo1_osc1_f_int*lfo1_out);
      osc1.set_volume_mod(lfo1_osc1_v_int*lfo1_out);
      osc2.set_freq_mod(lfo1_osc2_f_int*lfo1_out);
      osc2.set_volume_mod(lfo1_osc2_v_int*lfo1_out);

      filter.set_cutoff_mod(lfo1_cutoff_int*lfo1_out+filter_envmod*env.out);
      filter.set_reso_mod(lfo1_reso_int*lfo1_out);
    }
  }
  vTaskDelete(NULL);
}


void update_gui(bool conf_clicked, bool conf_long){
  // copy waveform display to local buffer
  uint8_t wf[128];
  memcpy(wf, (const void*)waveform[1-waveform_idx], 128*sizeof(uint8_t));

  // draw pixel by pixel (TODO: use internal image format)
  display.clear();
  for(int i=0; i<128; i++){
    display.setPixel(i, 16+(int16_t)wf[i]/6);
  }

  // add indicators
  display.setColor(WHITE);
  display.setFont((const uint8_t*)Lato_Regular_11);
  display.setTextAlignment(TEXT_ALIGN_LEFT);

  // current note
  char note[10];
  MidiConversion::get_name(MidiConversion::get_note(synth_config.osc1->get_freq()), note);
  display.drawString(1, 1, note);

  /*
  // current frequency
  char freq[40];
  snprintf(freq, 9, "%d", (int)input_frequency);
  freq[9] = '\0';
  display.drawString(1, 1, freq);
  */

  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(70, 1, "hgSynth");

  // wifi in the right corner
  if(WiFi.getMode()!=WIFI_OFF){
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(127, 1, "WiFi");
  }

  display.display();
}


void ui_handler(void * parameter){
  int slider = 0;
  bool btn = false;
  bool conf = false;
  bool long_press_triggered = false;

  uint32_t btn_last = 0;
  uint32_t conf_last = 0;
  uint32_t slider_last = 0;

  uint32_t ui_last = 0;

  const uint32_t debounce_time = 50;
  const uint32_t long_press = 2000;

  while(true){
    uint32_t tm = millis();
    bool b = !digitalRead(pin_btn);
    bool c = !digitalRead(pin_conf);
    int s = 0; //analogRead(pin_slider);

    bool btn_clicked = false;
    bool conf_clicked = false;
    bool conf_long = false;
    bool slider_changed = false;

    if(b!=btn && tm-btn_last>debounce_time){
      Serial.print("button ");
      Serial.println(b);
      button_state = b;
      btn = b;
      btn_last = tm;
      btn_clicked = true;
      //synth_config.env->gate(btn);
    }

    if(c!=conf && tm-conf_last>debounce_time){
      Serial.print("conf ");
      Serial.println(c);
      conf = c;
      conf_last = tm;
      long_press_triggered = false;
      conf_clicked = true;
      /*
      const int buff_size = 2*PITCH_TRACKER_BLOCK_SIZE; // must be power of 2
      int16_t buff[buff_size];
      memcpy(buff, pitch_buffer[(pitch_block_idx+1)%3], PITCH_TRACKER_BLOCK_SIZE*sizeof(int16_t));
      memcpy(buff+PITCH_TRACKER_BLOCK_SIZE, pitch_buffer[(pitch_block_idx+2)%3], PITCH_TRACKER_BLOCK_SIZE*sizeof(int16_t));
      Serial.println("\n");
      for(int i=0;i<buff_size; i++){
        Serial.print(buff[i]);
        Serial.print(", ");
      }
      Serial.println("");
      */
    }

    if(conf && tm-conf_last>long_press && !long_press_triggered){
      // conf button long press
      long_press_triggered = true;
      conf_long = true;
      Serial.println("long");
    }

    if(abs(s-slider)>4){
      Serial.print("slider ");
      Serial.println(s);
      slider = s;
      slider_changed = true;
    }

    // update UI
    update_gui(conf_clicked, conf_long);

    delay(50);
  }
}

void setup() {
  Serial.begin(115200);
  
  // buttons
  pinMode(pin_btn, INPUT);
  digitalWrite(pin_btn, HIGH);

  pinMode(pin_conf, INPUT);
  digitalWrite(pin_conf, HIGH);

  // init display
  pinMode(16, OUTPUT);
  digitalWrite(16, LOW);    // set GPIO16 low to reset OLED
  delay(50);
  digitalWrite(16, HIGH); // while OLED is running, must set GPIO16 in high  
  display.init();
  display.flipScreenVertically();

  display.clear();

  // SPIFFS for storing presets and wifi data
  if(!SPIFFS.begin(true)){
    Serial.println("SPIFFS Mount Failed");
  }

  // start the wifi if conf button pressed on boot
  if(digitalRead(pin_conf)){
    display.setFont((const uint8_t*)Lato_Regular_11);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 20, "WiFi starting");
    display.display();

    /*
    Serial.println("wifi connecting");
    WiFi.softAP(ssid, password);
    WiFi.softAPsetHostname(ssid);
    delay(400);
    */
    
    WiFi.mode(WIFI_STA);
    WiFi.begin(sta_ssid, sta_password);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
      Serial.printf("WiFi Failed!\n");
      return;
    }
    
    Serial.println("wifi started");
  }

  // attach AsyncWebSocket
  ws.onEvent(ws_handler);
  server.addHandler(&ws);
  server.on("/heap", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", String(ESP.getFreeHeap()));
  });
  server.on("/", HTTP_ANY, [](AsyncWebServerRequest *request){
    if(!request->authenticate(http_username, http_password))
        return request->requestAuthentication();
    request->send(200, "text/html", html_page);
  });
  server.begin();
  Serial.println("Server started");

  pinMode(BUILTIN_LED, OUTPUT);

  //initialize i2s with configurations above
  i2s_driver_install((i2s_port_t)i2s_num, &i2s_config, 0, NULL);
  i2s_set_pin((i2s_port_t)i2s_num, NULL);
  i2s_set_sample_rates((i2s_port_t)i2s_num, SAMPLE_RATE);

  /*
  // ADC settings
  analogReadResolution(12);             // Sets the sample bits and read resolution, default is 12-bit (0 - 4095), range is 9 - 12 bits
  analogSetWidth(12);                   // Sets the sample bits and read resolution, default is 12-bit (0 - 4095), range is 9 - 12 bits
                                        //  9-bit gives an ADC range of 0-511
                                        // 10-bit gives an ADC range of 0-1023
                                        // 11-bit gives an ADC range of 0-2047
                                        // 12-bit gives an ADC range of 0-4095
  analogSetCycles(8);                   // Set number of cycles per sample, default is 8 and provides an optimal result, range is 1 - 255
  analogSetSamples(1);                  // Set number of samples in the range, default is 1, it has an effect on sensitivity has been multiplied
  analogSetClockDiv(1);                 // Set the divider for the ADC clock, default is 1, range is 1 - 255
  analogSetAttenuation(ADC_0db);       // Sets the input attenuation for ALL ADC inputs, default is ADC_11db, range is ADC_0db, ADC_2_5db, ADC_6db, ADC_11db
  //analogSetPinAttenuation(pin_sound_input, ADC_0db); // Sets the input attenuation, default is ADC_11db, range is ADC_0db, ADC_2_5db, pin_sound_inputdb
                                        // ADC_0db provides no attenuation so IN/OUT = 1 / 1 an input of 3 volts remains at 3 volts before ADC measurement
                                        // ADC_2_5db provides an attenuation so that IN/OUT = 1 / 1.34 an input of 3 volts is reduced to 2.238 volts before ADC measurement
                                        // ADC_6db provides an attenuation so that IN/OUT = 1 / 2 an input of 3 volts is reduced to 1.500 volts before ADC measurement
                                        // ADC_11db provides an attenuation so that IN/OUT = 1 / 3.6 an input of 3 volts is reduced to 0.833 volts before ADC measurement
  adcAttachPin(pin_sound_input);
  adcAttachPin(pin_slider);
  //adcStart(pin_sound_input);
  //adcBusy(pin_sound_input);
  //adcEnd(pin_sound_input);
  */

  // setup ADC timer
  adc_timer = timerBegin(0, 80, true);  // increase every microsecond
  timerAttachInterrupt(adc_timer, &adc_update, true);
  timerAlarmWrite(adc_timer, 1000000/ADC_SAMPLE_RATE, true);
  timerAlarmEnable(adc_timer);

  Serial.println("init OK");

  // create block processing task
  xTaskCreatePinnedToCore(
      block_processor,  // Task function
      "BlockProc",      // String with name of task
      10000,            // Stack size in words
      NULL,             // Parameter passed as input of the task
      10,               // Priority of the task
      NULL,            // Task handle
      1);
  Serial.println("block processor created");
  
  xTaskCreatePinnedToCore(
      pitch_tracker,  // Task function
      "PitchTrack",      // String with name of task
      20000,            // Stack size in words
      NULL,             // Parameter passed as input of the task
      10,               // Priority of the task
      NULL,            // Task handle
      1);
  Serial.println("pitch tracker created");
  
  xTaskCreatePinnedToCore(
      ui_handler,       // Task function
      "UI",             // String with name of task
      10000,             // Stack size in words
      NULL,             // Parameter passed as input of the task
      1,                // Priority of the task
      NULL,            // Task handle
      0);
  Serial.println("ui handler created");


}

int bo = 0;
void loop() {
  delay(10);
  //adcStart(pin_slider);
  //pot = adcEnd(pin_slider);
  const int16_t _pot = analogRead(pin_slider);
  if(_pot!=pot){
    pot = _pot;
    Serial.printf("pot: %d\n", pot);
  }
  char t[20];
  //itoa(dac_buffer[dac_buffer_idx][0], t, 10);
  itoa(pot/8, t, 10);
  //display_big_num(t);

  if(input_tmp<2500){
    Serial.print(input_tmp);
    Serial.print("\t");
    Serial.println(input_frequency);
  }
  delay(50);

}
