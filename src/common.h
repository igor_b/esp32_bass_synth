#ifndef __COMMON_H__
#define __COMMON_H__

#include "oscillator.h"
#include "envelope.h"
#include "filter.h"

struct synth_data_t{
  Oscillator *osc1;
  Oscillator *osc2;
  bool *hard_sync;

  Oscillator *lfo1;
  float *lfo1_osc1_f_int;
  float *lfo1_osc1_v_int;
  float *lfo1_osc2_f_int;
  float *lfo1_osc2_v_int;
  float *lfo1_cutoff_int;
  float *lfo1_reso_int;

  Envelope *env;

  Filter *filter;
  float *filter_envmod;
};

#define BUILTIN_LED 25

#define BLOCK_SIZE 128
#define SAMPLE_RATE (int)48000

#define ADC_SAMPLE_RATE (int)15000
#define PITCH_TRACKER_BLOCK_SIZE (int)512
#define NBITS 32

// ------------------  pins
// display pins
static const int pin_display_sda = 4;
static const int pin_display_scl = 15;

// ui pins
static const int pin_slider = 36; // ADC2 interferes with wifi, must use ADC1
static const int pin_btn    = 14;
static const int pin_conf   = 17;

static const int pin_sound_input = 37; // ADC2 interferes with wifi, must use ADC1

// wifi stuff
static const char* sta_ssid = "dlink-3EF8";
static const char* sta_password = "jupyu01707";
static const char* ssid = "hgSynth";
static const char* password = "sawtooth1337";
static const char* http_username = "admin";
static const char* http_password = "admin";



#endif // __COMMON_H__