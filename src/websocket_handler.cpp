#include "websocket_handler.h"
#include "common.h"
#include "string.h"

extern volatile synth_data_t synth_config;

static void handle_param(char* req, AsyncWebSocketClient *client){
  // comm protocol:
  //   Q: status
  //   A: return all parameters in item_name/param_name=value format
  //
  //   Q: item_name/param_name=value
  //   A: set the value and respond OK, or ERR on invalid value
  if(strncmp(req, "gimmeall", 9)==0){
    // return all...

    client->printf("dump start");
    // oscillators
    char tmp[30];
    for(int i=0; i<2; i++){
      Oscillator *osc;
      if(i==0) osc = synth_config.osc1;
      else osc = synth_config.osc2;

      switch(osc->get_shape()){
        case OscillatorShape::SINE:
          strcpy(tmp, "sine");
          break;
        case OscillatorShape::TRIANGLE:
          strcpy(tmp, "triangle");
          break;
        case OscillatorShape::SAWTOOTH:
          strcpy(tmp, "sawtooth");
          break;
        case OscillatorShape::SQUARE:
          strcpy(tmp, "square");
          break;
      }
      client->printf("osc%d/shape=%s", i+1, tmp);
      client->printf("osc%d/offset=%d", i+1, osc->get_offset());
      client->printf("osc%d/detune=%d", i+1, osc->get_detune());
      client->printf("osc%d/volume=%d", i+1, (int)(osc->get_volume()*100));
    }
    client->printf("osc2/hard sync=%s", *synth_config.hard_sync?"true":"false");

    // envelope
    client->printf("env/attack=%d", (int)(synth_config.env->get_attack_time()*100));
    client->printf("env/decay=%d", (int)(synth_config.env->get_decay_time()*100));
    client->printf("env/sustain=%d", (int)(synth_config.env->get_sustain_level()*100));
    client->printf("env/release=%d", (int)(synth_config.env->get_release_time()*100));

    // lfo1 modulation
    switch(synth_config.lfo1->get_shape()){
      case OscillatorShape::SINE:
        strcpy(tmp, "sine");
        break;
      case OscillatorShape::TRIANGLE:
        strcpy(tmp, "triangle");
        break;
      case OscillatorShape::SAWTOOTH:
        strcpy(tmp, "sawtooth");
        break;
      case OscillatorShape::SQUARE:
        strcpy(tmp, "square");
        break;
    }
    client->printf("mod/lfo1 shape=%s", tmp);
    client->printf("mod/lfo1 frequency=%d", (int)(synth_config.lfo1->get_freq()*100));
    client->printf("mod/osc1 freq=%d",  (int)(*synth_config.lfo1_osc1_f_int*100));
    client->printf("mod/osc1 volume=%d",(int)(*synth_config.lfo1_osc1_v_int*100));
    client->printf("mod/osc2 freq=%d",  (int)(*synth_config.lfo1_osc2_f_int*100));
    client->printf("mod/osc2 volume=%d",(int)(*synth_config.lfo1_osc2_v_int*100));
    client->printf("mod/cutoff=%d",     (int)(*synth_config.lfo1_cutoff_int*100));
    client->printf("mod/resonance=%d",  (int)(*synth_config.lfo1_reso_int*100));

    // filter
    client->printf("filter/cutoff=%d", (int)(synth_config.filter->get_cutoff()*100));
    client->printf("filter/resonance=%d", (int)(synth_config.filter->get_reso()*100));
    client->printf("filter/env mod=%d", (int)(*synth_config.filter_envmod*100));

    // ...
    client->printf("dump end");
  }
  else if(strncmp(req, "save", 9)==0){
    // save params to flash
  }
  else{
    // tokenize
    char *item_name;
    char *param_name;
    char *value;
    char * saveptr;
    item_name = strtok_r(req, "/=", &saveptr);
    if(item_name==NULL){
      client->text("ERR: malformed input");
      return;
    }
    param_name = strtok_r(NULL, "/=", &saveptr);
    if(param_name==NULL){
      client->text("ERR: malformed input");
      return;
    }
    value = strtok_r(NULL, "/=", &saveptr);
    if(value==NULL){
      client->text("ERR: malformed input");
      return;
    }
    int val = atoi(value);
    if(item_name==NULL || param_name==NULL || value==NULL){
      client->text("ERR: malformed input");
      return;
    }

    // ------------------------------------------------- oscillators
    if(strncmp(item_name, "osc", 3)==0){
      const int idx=item_name[3];
      Oscillator *osc = item_name[3]=='1'?synth_config.osc1:synth_config.osc2;
      if(strncmp(param_name, "hard sync", 10)==0){
        // only on osc2
        *synth_config.hard_sync = value[0]=='t'?true:false;
      }
      else if(strncmp(param_name, "shape", 10)==0){
        if(strncmp(value, "sine", 10)==0) osc->set_shape(OscillatorShape::SINE);
        else if(strncmp(value, "triangle", 10)==0) osc->set_shape(OscillatorShape::TRIANGLE);
        else if(strncmp(value, "sawtooth", 10)==0) osc->set_shape(OscillatorShape::SAWTOOTH);
        else if(strncmp(value, "square", 10)==0) osc->set_shape(OscillatorShape::SQUARE);
        else{
          client->text("ERR: invalid shape");
          return;
        }
      }
      else if(strncmp(param_name, "offset", 10)==0){
        osc->set_offset(val);
      }
      else if(strncmp(param_name, "detune", 10)==0){
        osc->set_detune(val);
      }
      else if(strncmp(param_name, "volume", 10)==0){
        osc->set_volume((float)val/100.0);
      }
      else{
        client->text("ERR: unknown osc parameter");
        return;
      }
    }

    // ------------------------------------------------- envelope
    else if(strncmp(item_name, "env", 10)==0){
      if(strncmp(param_name, "attack", 10)==0){
        synth_config.env->set_attack_time((float)val/100.0);
      }
      else if(strncmp(param_name, "decay", 10)==0){
        synth_config.env->set_decay_time((float)val/100.0);
      }
      else if(strncmp(param_name, "sustain", 10)==0){
        synth_config.env->set_sustain_level((float)val/100.0);
      }
      else if(strncmp(param_name, "release", 10)==0){
        synth_config.env->set_release_time((float)val/100.0);
      }
      else if(strncmp(param_name, "gate", 10)==0){
        synth_config.env->gate(value[0]=='t'?true:false);
      }
      else{
        client->text("ERR: unknown envelope parameter");
      }
    }

    // ------------------------------------------------- modulations
    else if(strncmp(item_name, "mod", 10)==0){
      if(strncmp(param_name, "lfo1 shape", 20)==0){
        if(strncmp(value, "sine", 10)==0)          synth_config.lfo1->set_shape(OscillatorShape::SINE);
        else if(strncmp(value, "triangle", 10)==0) synth_config.lfo1->set_shape(OscillatorShape::TRIANGLE);
        else if(strncmp(value, "sawtooth", 10)==0) synth_config.lfo1->set_shape(OscillatorShape::SAWTOOTH);
        else if(strncmp(value, "square", 10)==0)   synth_config.lfo1->set_shape(OscillatorShape::SQUARE);
        else{
          client->text("ERR: invalid lfo shape");
          return;
        }
      }
      else if(strncmp(param_name, "lfo1 frequency", 20)==0){
        synth_config.lfo1->set_freq((float)val/10.0);
      }
      else if(strncmp(param_name, "osc1 freq", 20)==0){
        *synth_config.lfo1_osc1_f_int = (float)val/100.0;
      }
      else if(strncmp(param_name, "osc1 volume", 20)==0){
        *synth_config.lfo1_osc1_v_int = (float)val/100.0;
      }
      else if(strncmp(param_name, "osc2 freq", 20)==0){
        *synth_config.lfo1_osc2_f_int = (float)val/100.0;
      }
      else if(strncmp(param_name, "osc2 volume", 20)==0){
        *synth_config.lfo1_osc2_v_int = (float)val/100.0;
      }
      else if(strncmp(param_name, "cutoff", 20)==0){
        *synth_config.lfo1_cutoff_int = (float)val/100.0;
      }
      else if(strncmp(param_name, "resonance", 20)==0){
        *synth_config.lfo1_reso_int = (float)val/100.0;
      }
      else{
        client->text("ERR: unknown mod parameter");
      }
    }

    // ------------------------------------------------- filter
    else if(strncmp(item_name, "filter", 10)==0){
      if(strncmp(param_name, "cutoff", 10)==0){
        synth_config.filter->set_cutoff((float)val/100.0);
      }
      else if(strncmp(param_name, "resonance", 10)==0){
        synth_config.filter->set_reso((float)val/100.0);
      }
      else if(strncmp(param_name, "env mod", 10)==0){
        *synth_config.filter_envmod = (float)val/100.0;
      }
      else{
        client->text("ERR: unknown filter parameter");
        return;
      }
    }
    else{
      client->text("ERR: unknown item name");
      return;
    }
    //client->text("OK");
  }
}


// websocket event handling
void ws_handler(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
  if(type == WS_EVT_CONNECT){
    //client connected
    printf("ws[%s][%u] connect\n", server->url(), client->id());
    client->text("yo");
  }
  else if(type == WS_EVT_DISCONNECT){
    //client disconnected
    printf("ws[%s][%u] disconnect: %u\n", server->url(), client->id());
  }
  else if(type == WS_EVT_ERROR){
    //error was received from the other end
    printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
  }
  else if(type == WS_EVT_DATA){
    //data packet
    AwsFrameInfo * info = (AwsFrameInfo*)arg;
    if(info->final && info->index == 0 && info->len == len){
      // the whole message is in a single frame and we got all of it's data
      // we assume it's a text message
      printf("ws[%s][%u] %s-message[%llu]: ", server->url(), client->id(), (info->opcode == WS_TEXT)?"text":"binary", info->len);
      if(info->opcode == WS_TEXT){
        data[len] = 0;
        printf("%s\n", (char*)data);        /// !!!
        handle_param((char*)data, client);
      }
    } 
    else{
        printf("multiframe messages not supported");
        client->text("ERR: multiframe messages not supported");
    }
  }
}
