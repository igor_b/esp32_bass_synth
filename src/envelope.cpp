#include "envelope.h"
#include <math.h>

Envelope::Envelope(int sample_rate){
  out = 0;
  this->sample_rate = sample_rate;
  set_attack_time(0.5);
  set_decay_time(0.1);
  set_release_time(0.5);
  set_sustain_level(0.8);
  set_target_ratio_A(0.3);
  set_target_ratio_DR(0.3);
}

void Envelope::set_attack_time(float time){
  attack_time = time;
  attack_rate = time*(float)sample_rate;
  attack_coeff = calc_coeff(attack_rate, target_ratio_A);
  attack_base = (1.0 + target_ratio_A) * (1.0 - attack_coeff);
}

void Envelope::set_decay_time(float time){
  decay_time = time;
  decay_rate = time*(float)sample_rate;
  decay_coeff = calc_coeff(decay_rate, target_ratio_DR);
  decay_base = (sustain_level - target_ratio_DR) * (1.0 - decay_coeff);
}

void Envelope::set_release_time(float time){
  release_time = time;
  release_rate = time*(float)sample_rate;
  release_coeff = calc_coeff(release_rate, target_ratio_DR);
  release_base = -target_ratio_DR * (1.0 - release_coeff);
}

float Envelope::calc_coeff(float rate, float target_ratio){
  return (rate <= 0) ? 0.0 : exp(-log((1.0 + target_ratio) / target_ratio) / rate);
}

void Envelope::set_sustain_level(float level){
  sustain_level = level;
  decay_base = (sustain_level - target_ratio_DR) * (1.0 - decay_coeff);
}

void Envelope::set_target_ratio_A(float target_ratio){
  if (target_ratio < 0.000000001)
    target_ratio = 0.000000001; // -180 dB
  target_ratio_A = target_ratio;
  attack_coeff = calc_coeff(attack_rate, target_ratio_A);
  attack_base = (1.0 + target_ratio_A) * (1.0 - attack_coeff);
}

void Envelope::set_target_ratio_DR(float target_ratio){
  if (target_ratio < 0.000000001)
    target_ratio = 0.000000001; // -180 dB
  target_ratio_DR = target_ratio;
  decay_coeff = calc_coeff(decay_rate, target_ratio_DR);
  release_coeff = calc_coeff(release_rate, target_ratio_DR);
  decay_base = (sustain_level - target_ratio_DR) * (1.0 - decay_coeff);
  release_base = -target_ratio_DR * (1.0 - release_coeff);
}
