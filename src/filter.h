#ifndef __FILTER_H__
#define __FILTER_H__

#include <math.h>

// Simple resonant filter
//
// Design by Paul Kellet: http://musicdsp.org/showone.php?id=29
//
// This file is part of esp32 bass synth project
// (c)2018 Igor Brkic <igor@hyperglitch.com>

class Filter{
  float buf0;
  float buf1;
  float fb;
  float fc;
  float reso;

  float cutoff_mod;
  float reso_mod;

  inline void __recalc(){
    float r = reso+reso_mod;
    if(r<0) r=0;
    else if(r>0.99) r=0.99;

    float f = fc+cutoff_mod;
    if(f<0) f=0;
    else if(f>0.99) f=0.99;
    
    fb = r + r / (1.0 - f);
  }

public:
  float out;

  Filter(float cutoff=0.0, float reso=0.0){
    buf0 = 0.0;
    buf1 = 0.0;
    this->fc = cutoff;
    this->reso = reso;
    out = 0.0;
    cutoff_mod = 0;
    reso_mod = 0;
    __recalc();
  }

  inline void set_cutoff(float cutoff){
    // prevent recalculation for small changes
    // TODO: add fc smoothing
    if(fabs(fc-cutoff)<0.005) return;
    this->fc = cutoff;
    __recalc();
  }
  float get_cutoff(){
    return fc;
  }
  
  inline void set_reso(float reso){
    this->reso = reso;  
    __recalc();
  }
  float get_reso(){
    return reso;
  }

  inline void set_cutoff_mod(float mod){
    cutoff_mod = mod;
    __recalc();
  }
  inline void set_reso_mod(float mod){
    reso_mod = mod;
    __recalc();
  }

  inline float run(float in){
    buf0 += fc * (in - buf0 + fb * (buf0 - buf1));
    buf1 += fc * (buf0 - buf1);
    out = buf1;
    return out;
  }
};

#endif  // __FILTER_H__