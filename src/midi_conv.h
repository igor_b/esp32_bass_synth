#ifndef __MIDI_CONV_H__
#define __MIDI_CONV_H__

#include <math.h>
#include <stdio.h>

// Midi note conversion functions
//
// This file is part of esp32 bass synth project
// (c)2018 Igor Brkic <igor@hyperglitch.com>

static float freqs[127];
static const char names[12][5] = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "Bb", "B"};
static bool initialized = false;

class MidiConversion{
  static void init(){
    // calculate the conversion tables
    const float a = 440.0;
    for(int i=0; i<127; i++){
      freqs[i] = (a/32.0) * pow(2, ((i - 9) / 12.0));
    }
    initialized = true;
  }

public:
  static float get_freq(const int note){
    if(!initialized) MidiConversion::init();
    return freqs[note];
  }

  static int get_note(const float freq){
    if(!initialized) MidiConversion::init();
    int i;
    for(i=1; i<126; i++){
      if(freqs[i]>freq) break;
    }
    // quantize to note
    const float freq_prev = freqs[i-1];
    const float freq_curr = freqs[i];
    if(freq_curr-freq < freq-freq_prev){
      return i;
    }
    return i-1;
  }

  static void get_name(const int note, char *name){
    if(!initialized) MidiConversion::init();
    sprintf(name, "%s%d", names[note%12], note/12);
  }

};

#endif  // __MIDI_CONV_H__