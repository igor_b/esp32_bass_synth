#ifndef __ENV_H__
#define __ENV_H__

// Envelope generator
//
// Design by Nigel Redmon: http://www.earlevel.com/main/2013/06/03/envelope-generators-adsr-code/
//
// This file is part of esp32 bass synth project
// (c)2018 Igor Brkic <igor@hyperglitch.com>


enum EnvelopeState{
  IDLE,
  ATTACK,
  DECAY,
  SUSTAIN,
  RELEASE
};

class Envelope{
  EnvelopeState state;
  int sample_rate;
  float attack_rate;
  float decay_rate;
  float release_rate;
  float sustain_level;
  float attack_coeff;
  float decay_coeff;
  float release_coeff;

  float attack_time;
  float decay_time;
  float release_time;

  float target_ratio_A;
  float target_ratio_DR;

  float attack_base;
  float decay_base;
  float release_base;

  float calc_coeff(float rate, float target_ratio);

public:
  float out;
  bool retrig = true;

  Envelope(int sr);

  inline float process(){
    switch(state){
    case EnvelopeState::IDLE:
      break;
    case EnvelopeState::ATTACK:
      out = attack_base + out*attack_coeff;
      if(out >= 1.0){
        out = 1.0;
        state = EnvelopeState::DECAY;
      }
      break;
    case EnvelopeState::DECAY:
      out = decay_base + out*decay_coeff;
      if(out <= sustain_level){
        out = sustain_level;
        state = EnvelopeState::SUSTAIN;
      }
      break;
    case EnvelopeState::SUSTAIN:
      break;
    case EnvelopeState::RELEASE:
      out = release_base + out*release_coeff;
      if(out <= 0.0){
        out = 0.0;
        state = EnvelopeState::IDLE;
      }
    }
    return out;
  }

  inline void gate(bool g){
    if(g){
      state = EnvelopeState::ATTACK;
    }
    else if(state!=EnvelopeState::IDLE){
      state = EnvelopeState::RELEASE;
    }
  }

  inline EnvelopeState get_state(){
    return state;
  }

  void set_attack_time(float time);
  void set_decay_time(float time);
  void set_sustain_level(float sl);
  void set_release_time(float time);

  float get_attack_time(){
    return attack_time;
  }
  float get_decay_time(){
    return decay_time;
  }
  float get_release_time(){
    return release_time;
  }
  float get_sustain_level(){
    return sustain_level;
  }

  void set_target_ratio_A(float tr);
  void set_target_ratio_DR(float tr);
};

#endif  // __ENV_H__