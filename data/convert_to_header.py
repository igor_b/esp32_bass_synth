#!/usr/bin/env python

import re
import sys

"""
convert html file to C char array
used to add html page to ESP32's source without the need for separate upload to SPIFFS

this file is part of esp32 bass synth project
(c)2018 Igor Brkic <igor@hyperglitch.com>
"""

def main():
    if len(sys.argv)!=3:
        print("Usage: %s <input.html> <output.h>" % sys.argv[0])

    with open(sys.argv[1], 'r') as html:
        with open(sys.argv[2], 'w') as out:
            guard = '__'+re.sub(r'[^A-Z]+', '_', sys.argv[2].upper())+'__'
            out.write("#ifndef %s\n#define %s\n\n"%(guard, guard))
            out.write("// created from file %s using data/convert_to_header.py script\n\n" % (sys.argv[1],))
            out.write("const char * html_page = \"")

            # remove leading whitespace
            remove_whitespace = False
            while True:
                c=html.read(1)
                if c=='':
                    break
                if c=='\n':
                    out.write('\\n')
                    remove_whitespace = True
                elif c in (' ', '\t') and remove_whitespace:
                    pass
                elif c in ('\\', '"'):
                    remove_whitespace = False
                    out.write('\\'+c)
                else:
                    remove_whitespace = False
                    out.write(c)
            out.write("\";\n")

            out.write("\n#endif   // %s"%(guard,))

if __name__=='__main__':
    main()


